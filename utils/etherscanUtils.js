var etherscanKey = require("../secrets.json").etherscanKey;
var api = require("etherscan-api").init(etherscanKey);

function requestAddressData(address) {
  var balancePromise = api.account.balance(address);
  var transactionsPromise = api.account.txlist(address);

  return Promise.all([
    balancePromise,
    transactionsPromise.catch(error => {
      // have to special case this because for some reason 
      // the etherscan api treats 0 transactions as an error
      if (error === "No transactions found") {  
        return false;
      }
    })
  ]);
}

module.exports = {
  requestAddressData
};

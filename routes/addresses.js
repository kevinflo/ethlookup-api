var express = require("express");
var router = express.Router();
var etherscanUtils = require("../utils/etherscanUtils");

/* GET address data. Persist to DB and respond with JSON */
router.get("/:address", function(req, res, next) {
  var requestAddressDataPromise = etherscanUtils.requestAddressData(
    req.params.address
  );

  requestAddressDataPromise
    .then(function(addressData) {
      var balance = addressData[0].result;

      // For the special case of no transactions just use an empty array
      // since etherscan returns an error instead of that
      var transactions = addressData[1].result || [];

      var filterAddress = req.query.filter;
      if (filterAddress) {
        transactions = transactions.filter(function(transaction) {
          return (
            transaction.to === filterAddress ||
            transaction.from === filterAddress
          );
        });
      }

      res.json({ id: req.params.id, balance, transactions });
    })
    .catch(function(error) {
      res.status(500).json({ error: error });
    });
});

module.exports = router;

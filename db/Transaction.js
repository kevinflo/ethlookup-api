var sequelize = require("./sequelizeInit");
var Sequelize = require("sequelize");

const Transaction = sequelize.define("account", {
  hash: {
    type: Sequelize.STRING,
    validate: {
      len: [65, 67]
    }
  },
  blockHash: {
    type: Sequelize.STRING
  },
  blockNumber: { type: Sequelize.INTEGER },
  confirmations: { type: Sequelize.INTEGER },
  contractAddress: { type: Sequelize.STRING },
  cumulativeGasUsed: { type: Sequelize.INTEGER },
  from: {
    type: Sequelize.STRING,
    validate: {
      len: [41, 43]
    }
  },
  gas: { type: Sequelize.INTEGER },
  gasPrice: { type: Sequelize.INTEGER },
  gasUsed: { type: Sequelize.INTEGER },
  input: { type: Sequelize.STRING },
  isError: { type: Sequelize.INTEGER },
  nonce: { type: Sequelize.INTEGER },
  timeStamp: { type: Sequelize.INTEGER },
  to: {
    type: Sequelize.STRING,
    validate: {
      len: [41, 43]
    }
  },
  transactionIndex: { type: Sequelize.INTEGER },
  txreceipt_status: { type: Sequelize.INTEGER },
  value: { type: Sequelize.INTEGER },
  indexes: [
    // Create a unique index on hash
    {
      unique: true,
      fields: ["hash"]
    }
  ]
});

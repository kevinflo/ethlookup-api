const Sequelize = require('sequelize');
var secrets = require("../secrets.json");
const sequelize = new Sequelize('ethlookup', secrets.database.username, secrets.database.password, {
  host: 'localhost',
  dialect: 'postgres',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});
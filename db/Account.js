var sequelize = require("./sequelizeInit");

const User = sequelize.define("account", {
  hash: {
    type: Sequelize.STRING,
    validate: {
      len: [41, 43]
    }
  },
  balance: {
    type: Sequelize.INTEGER
  },
  indexes: [
    // Create a unique index on hash
    {
      unique: true,
      fields: ["hash"]
    }
  ]
});